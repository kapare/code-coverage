#include <stdio.h>

int main()
{
	int i;
	int total;

	for (i = 0; i < 10; i++)
		total += i;

	if (total != 45)
		printf("Fail\n");
	else
		printf("Success\n");
	return 0;
}
