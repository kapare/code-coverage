# Code Coverage

## Description

Tools that help to see the code coverage.

## GCOV

`gcov` is a test coverage program.

### Compilation 

```
gcc -fprofile-arcs -ftest-coverage gcc-gcov-example/tmp.c
```

This will generate `a.out` and `tmp.gcno`.

### Running

```
./a.out
```

This will generate `tmp.gcda`.

### Test coverage

```
gcov tmp.c
```

Output:

```
File 'gcc-gcov-example/tmp.c'
Lines executed:85.71% of 7
Creating 'tmp.c.gcov'
```

### Analyze Coverage

```
vim tmp.c.gcov
```

### References

https://gcc.gnu.org/onlinedocs/gcc/Invoking-Gcov.html#Invoking-Gcov

### GCOVR

The gcovr command provides a utility for managing the use of the GNU gcov utility and generating summarized code coverage results.

#### Install

```
sudo apt install python-pip
sudo pip install gcovr
```

#### Usage

`gcovr` can be used as replacement of `gcov` to generate `HTML` file.

```
gcovr -r . --html --html-details -o example2.html
```

#### Reference

http://gcovr.com

## CMake Code Coverage

The [CMakeLists.txt](CMakeLists.txt) have a target for `coverage`:

```
cd build
cmake -DCMAKE_BUILD_TYPE=Debug ..
make coverage
```

To visalize the output:

```
chromium-browser coverage_report/index.html
```

### Reference

https://gist.github.com/dikaiosune/ad46dcba65bff11907cd473c3ed94542

https://github.com/bilke/cmake-modules/blob/master/CodeCoverage.cmake
